{ config, pkgs, ... }:

{
  imports = [
    ./configuration.nix
  ];
  
  networking.hostName = "nixron"; # Define your hostname.

  fileSystems = {
    "/mx500".options = [ "compress=zstd" ];
    "/home/aaron/Games" = {
	device = "/mx500/aaron/Games";
	options = [ "bind" ];
    };
  };
}

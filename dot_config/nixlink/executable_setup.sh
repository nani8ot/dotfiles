#!/usr/bin/env sh

hosts="nixpad nixron"

# check if running as root
if [ $(id -u) -ne 0 ] ; then
	echo "Please run as root"
	exit 1
fi

for name in $hosts ; do
	if [ "$(hostname)" = "$name" ] ; then
		echo Is $(hostname) the correct system? Press Enter to symlink to /etc/nixos/configuration.nix.
		read
		sec_date=$(date +%s)
		mv "/etc/nixos/configuration.nix" "/etc/nixos/configuration.nix.bkp-$sec_date"
		ln -s "$(pwd)/configuration-$(hostname).nix" "/etc/nixos/configuration.nix"
		echo "Moved existing configuration.nix to configuration.nix.bkp-$sec_date"
		echo "Symlinked $(pwd)/configuration-$(hostname).nix to /etc/nixos/configuration.nix"
		exit 0
	fi
done
echo "Host $(hostname) not in list. Aborting."
exit 1

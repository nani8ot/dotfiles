# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  # enable unstable nixpkgs with the unstable.xyz keyword
  unstablepkgs = import <unstable> {};

  # bash script to let dbus know about important env variables and
  # propagate them to relevent services run at the end of sway config
  # see
  # https://github.com/emersion/xdg-desktop-portal-wlr/wiki/"It-doesn't-work"-Troubleshooting-Checklist
  # note: this is pretty much the same as  /etc/sway/config.d/nixos.conf but also restarts  
  # some user services to make sure they have the correct environment variables
  dbus-sway-environment = pkgs.writeTextFile {
    name = "dbus-sway-environment";
    destination = "/bin/dbus-sway-environment";
    executable = true;

    text = ''
  dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=sway
  systemctl --user stop pipewire wireplumber xdg-desktop-portal xdg-desktop-portal-wlr
  systemctl --user start pipewire wireplumber xdg-desktop-portal xdg-desktop-portal-wlr
      '';
  };

  # currently, there is some friction between sway and gtk:
  # https://github.com/swaywm/sway/wiki/GTK-3-settings-on-Wayland
  # the suggested way to set gtk settings is with gsettings
  # for gsettings to work, we need to tell it where the schemas are
  # using the XDG_DATA_DIR environment variable
  # run at the end of sway config
  configure-gtk = pkgs.writeTextFile {
      name = "configure-gtk";
      destination = "/bin/configure-gtk";
      executable = true;
      text = let
        schema = pkgs.gsettings-desktop-schemas;
        datadir = "${schema}/share/gsettings-schemas/${schema.name}";
      in ''
        export XDG_DATA_DIRS=${datadir}:$XDG_DATA_DIRS
        gnome_schema=org.gnome.desktop.interface
        gsettings set $gnome_schema gtk-theme 'Adwaita-dark'
        '';
  };

  flameshotPin = import (pkgs.fetchFromGitHub {
    owner = "haizaar";
    repo = "nixpkgs";
    rev = "ca6081cbce02589ad9594350ce6e5bca39b09ad1";
    sha256 = "6lOu2O4KAYfGccHdZQUEr2A3mdwEtcr3S7pyLxxlZII=";
  }) {};

in
{
  imports =
    [ # Include the results of the hardware scan.
      /etc/nixos/hardware-configuration.nix
      <home-manager/nixos>
    ];

  # Setup keyfile
  boot.initrd.secrets = {
    "/crypto_keyfile.bin" = null;
  };

  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  networking.hostName = "nixpad"; # Define your hostname.

  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  i18n.extraLocaleSettings = {
    LC_ADDRESS = "de_DE.UTF-8";
    LC_IDENTIFICATION = "de_DE.UTF-8";
    LC_MEASUREMENT = "de_DE.UTF-8";
    LC_MONETARY = "de_DE.UTF-8";
    LC_NAME = "de_DE.UTF-8";
    LC_NUMERIC = "de_DE.UTF-8";
    LC_PAPER = "de_DE.UTF-8";
    LC_TELEPHONE = "de_DE.UTF-8";
    LC_TIME = "de_DE.UTF-8";
  };

  # Configure keymap in X11
  services.xserver = {
    layout = "de";
    xkbVariant = "koy";
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.aaron = {
    isNormalUser = true;
    description = "Aaron";
    extraGroups = [ "networkmanager" "wheel" "video" "wireshark" ];
    packages = with pkgs; [];
  };

  home-manager.users.aaron = { pkgs, ... }: {
    home.stateVersion = "22.11";
    home.packages = [
      pkgs.chezmoi
      pkgs.foot
      pkgs.nix-du # visualize how much space nix store uses
      pkgs.distrobox # requires podman
      pkgs.pika-backup # flatpak doesn't work https://github.com/NixOS/nixpkgs/issues/138956
      pkgs.tree
      pkgs.killall
      pkgs.git
      pkgs.btop
      pkgs.nethogs
      pkgs.iftop
      # # It is sometimes useful to fine-tune packages, for example, by applying
      # # overrides. You can do that directly here, just don't forget the
      # # parentheses. Maybe you want to install Nerd Fonts with a limited number of
      # # fonts?
      # (pkgs.nerdfonts.override { fonts = [ "FantasqueSansMono" ]; })
      # # You can also create simple shell scripts directly inside your
      # # configuration. For example, this adds a command 'my-hello' to your
      # # environment:
      # (pkgs.writeShellScriptBin "my-hello" ''
      #   echo "Hello, ${config.home.username}!"
      # '')
    ];
    programs.git = {
      enable = true;
      userName = "nani8ot";
      userEmail = "git@wiedemer.xyz";
      aliases = {
        st = "status";
      };
    };
    programs.neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
      extraConfig = ''
        set number relativenumber
	autocmd FileType * :TSEnable highlight
	'';
      plugins = [
        pkgs.vimPlugins.nvim-treesitter
	pkgs.vimPlugins.completion-treesitter
	pkgs.vimPlugins.nvim-treesitter.withAllGrammars
      ];
    };
    programs.fish = {
      enable = true;
      interactiveShellInit = ''
        set fish_greeting # Disable greeting
      '';
    };
  };

  # Allow unfree packages
  nixpkgs.config.allowUnfree = true;

  # Enable Flakes
  nix.settings.experimental-features = "nix-command flakes";
  nix.package = pkgs.nixFlakes;

  # Environment
  
  # set default editor
  environment.variables.EDITOR = "nvim";

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #  vim # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    wget
    neovim
    dbus-sway-environment
    configure-gtk
    wayland
    xdg-utils # for opening default programs when clicking links
    glib # gsettings
    gnome3.adwaita-icon-theme  # default gnome cursors
    swaylock
    swayidle
    grim # screenshot functionality
    slurp # screenshot functionality
    wl-clipboard # wl-copy and wl-paste for copy/paste from stdin / stdout
    rofi-wayland
    mako # notification system developed by swaywm maintainer
    #dunst
    wdisplays # tool to configure displays
    flameshotPin.flameshot # added in unstable
    waybar
    ulauncher
    kanshi
    unstablepkgs.cliphist # added in unstable
    blueman
    pamixer
    lxqt.lxqt-policykit
    pcmanfm
    gnome.nautilus
    gnome.gnome-software
    papirus-icon-theme
    libnotify
    networkmanagerapplet
    playerctl
  ];

  fonts.fonts = with pkgs; [
    font-awesome
    fira-code
  ];

  # enable sway window manager
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
  };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;

  # zram, default 1/2 total RAM
  zramSwap.enable = true;

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable flatpak
  xdg.portal = {
    enable = true;
    wlr.enable = true;
    # gtk portal needed to make gtk apps happy
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  };
  services.flatpak.enable = true;
  
  # Enable Gnome Display Manager
  services.xserver.enable = true;
  services.xserver.displayManager = {
    gdm.enable = true;
    gdm.wayland = true;
    defaultSession = "sway";
    autoLogin.user = "aaron";
    autoLogin.enable = true;
  };

  # Enable gnome-keyring
  services.gnome.gnome-keyring.enable = true;
  programs.seahorse.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };

  # Enable bluetooth
  hardware.bluetooth.enable = true;
  services.blueman.enable = true;

  # power management
  # enable tlp
  services.tlp.enable = true;
  services.tlp.settings = {
    START_CHARGE_THRESH_BAT0 = 75;
    STOP_CHARGE_THRESH_BAT0 = 80;
  };

  # Virtualisation
  # enable podman for distrobox
  virtualisation = {
    podman = {
      enable = true;

      # Create a `docker` alias for podman, to use it as a drop-in replacement
      dockerCompat = true;

      # Required for containers under podman-compose to be able to talk to each other.
      #defaultNetwork.settings.dns_enabled = true; # nix-unstable
      defaultNetwork.dnsname.enable = true; # nix-22.11
    };
  };


  # security
  # enable polkit
  security.polkit.enable = true;

  security.sudo.extraRules = [
    # Allow execution of any command by all users in group sudo,
    # requiring a password.
    #{ groups = [ "sudo" ]; commands = [ "ALL" ]; }

    # Allow execution of "/home/root/secret.sh" by user `backup`, `database`
    # and the group with GID `1006` without a password.
    { users = [ "aaron" ]; groups = [ "wheel" ];
      commands = [ 
        { command = "sudoedit /etc/nixos/configuration.nix"; options = [ "SETENV" "NOPASSWD" ]; } 
        { command = "/run/current-system/sw/bin/nixos-rebuild switch"; options = [ "SETENV" "NOPASSWD" ]; } 
        { command = "/home/aaron/.nix-profile/bin/nethogs"; options = [ "SETENV" "NOPASSWD" ]; } 
        { command = "/home/aaron/.nix-profile/bin/iftop"; options = [ "SETENV" "NOPASSWD" ]; } 
      ];
    }
  ];

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?

}
